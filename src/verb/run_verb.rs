use std::fs;
use std::process::Command;
use structopt::{StructOpt};
use threadpool::ThreadPool;
use regex::Regex;

#[derive(StructOpt, Debug)]
pub struct RunVerb {
    #[structopt(long="ith")]
    ith: usize,

    #[structopt(long="idth")]
    idth: usize,

    #[structopt(long="delta")]
    delta: usize,
}

impl RunVerb {
    pub fn run(&self) {
        let ith = self.ith;
        let idth = self.idth;
        let delta = self.delta;

        let max_workers_num = if delta > num_cpus::get() - 4 {
            print!("cpu:{},", num_cpus::get());
            panic!("error: number of cpu is less than max number of workers.");
        } else {
            delta
        };

        let pool: ThreadPool = threadpool::Builder::new()
            .num_threads(max_workers_num)
            .thread_stack_size(4_000_000)
            .build();
        let (nsize, leaf_level) = replace_param();

        let mut processed_index = ith;
        while processed_index <= idth {
            let worker_num = if max_workers_num > (idth - processed_index + 1) {
                idth - processed_index + 1
            } else {
                max_workers_num
            };

            for i in 0..worker_num {
                pool.execute(move || {
                    create_experiment_filename(nsize, leaf_level, processed_index + i);
                    launch_process(nsize, leaf_level, processed_index + i);
                });
            }
            pool.join();
            processed_index += worker_num;
        }
    }
}

pub fn replace_param() -> (usize, usize) {
    // Read file and split content
    let filename = "./cbts/inc/type_ts.h";
    let src_code = fs::read_to_string(filename).expect("Could not read file");

    (
        extract_number(&src_code, r"#define NSIZE [1-9]*"),
        extract_number(&src_code, r"#define LEAF_LEVEL [1-9]*")
    )
}

pub fn extract_number(src_code: &String, re: &str) -> usize {
    let re1 = Regex::new(re).unwrap();
    let line = re1.captures(&src_code).unwrap().get(0).map_or("", |m| m.as_str());

    let num_str = line.split(' ').collect::<Vec<&str>>()[2];
    num_str.parse::<usize>().unwrap()
}

fn create_experiment_filename(nsize: usize, leaf_level: usize, ith: usize) {
    let exp_name = format!("n{}-lv{}", nsize, leaf_level);
    let logpath = format!("./cbts/{}/{}-{}.log", exp_name, exp_name, ith);
    let jsonpath = format!("./cbts/{}/{}-{}.json", exp_name, exp_name, ith);
    fs::File::create(logpath).expect("could not create file");
    fs::File::create(jsonpath).expect("could not create file");
}

fn launch_process(nsize: usize, leaf_level: usize, ith: usize) {
    let exp_name = format!("n{}-lv{}", nsize, leaf_level);
    let logpath = format!("./{}/{}-{}.log", exp_name, exp_name, ith);
    let jsonpath = format!("./{}/{}-{}.json", exp_name, exp_name, ith);

    let mut child = Command::new("./bts/bts");
    child.arg(format!("{}", ith))
    .arg(logpath)
    .arg(jsonpath);
    child.current_dir("./cbts");
    child.status().expect("process failed to execute");
}
