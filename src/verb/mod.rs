use std::process::Command;
use structopt::{StructOpt, clap};

mod build_verb;
use build_verb::BuildVerb;

mod run_verb;
use run_verb::RunVerb;

#[derive(StructOpt, Debug)]
#[structopt(
    name=clap::crate_name!(),
    version=clap::crate_version!(),
    author=clap::crate_authors!(),
    about=clap::crate_description!(),
)]
pub enum Verb {
    Build(BuildVerb),
    Run(RunVerb),
}

pub(self) fn run_prog(cmd: &str, current_dir: &str, error_message: &str) {
    let mut child = Command::new(cmd);
    child.current_dir(current_dir);
    child.status().expect(error_message);
}

pub(self) fn run_prog_with_args(cmd: &str, arg: &String, current_dir: &str, error_message: &str) {
    let mut child = Command::new(cmd);
    child.arg(arg);
    child.current_dir(current_dir);
    child.status().expect(error_message);
}
