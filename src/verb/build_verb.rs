use std::fs;
use structopt::{StructOpt};
use regex::Regex;
use super::run_prog;

#[derive(StructOpt, Debug)]
pub struct BuildVerb {
    #[structopt(long="nsize")]
    nsize: usize,
    #[structopt(long="leaf")]
    leaf_level: usize,
}

impl BuildVerb {
    // initial directory ksr/.
    pub fn run(&self) {
        Self::replace_param(self.nsize, self.leaf_level);
        // build preset binary
        run_prog("make", "./cbts/setupper", "db creator failed");
        // create log directory and root-n-lv.log
        Self::create_log_area(self.nsize, self.leaf_level);
        // build preset data collection
        let logfilepath = format!("./n{}-lv{}/root-n{}-lv{}.log", self.nsize, self.leaf_level, self.nsize, self.leaf_level);
        super::run_prog_with_args("./setupper/setupper", &logfilepath, "./cbts", "db creation failed");
        // build searcher
        run_prog("make", "./cbts/bts", "ks binary creation failed");
    }

    pub fn replace_param(nsize: usize, leaf_level: usize) {
        // Read file and split content
        let filename = "./cbts/inc/type_ts.h";
        let mut src_code = fs::read_to_string(filename).expect("Could not read file");

        let find_and_replace = [
            (r"#define NSIZE [1-9]*", format!(r"#define NSIZE {}", nsize)),
            (r"#define LEAF_LEVEL [1-9]*", format!(r"#define LEAF_LEVEL {}", leaf_level)),
        ];

        for (reg, replace) in find_and_replace.iter() {
            src_code = Self::replace_text(&src_code, reg, replace);
        }

        match fs::write(filename, src_code) {
            Ok(()) => (),
            Err(e) => panic!("{}", e),
        }
    }

    fn replace_text(text: &str, re: &str, replaced_str: &str) -> String {
        let reg = Regex::new(re).unwrap();
        format!("{}", reg.replace(text, replaced_str))
    }

    pub fn create_log_area(nsize: usize, leaf_level: usize) {
        let dirpath = format!("./cbts/n{}-lv{}", nsize, leaf_level);
        let nsize_src_filepath = format!("{}/root-n{}-lv{}.log", dirpath, nsize, leaf_level);
        fs::create_dir_all(dirpath).expect("could not create dir");
        fs::File::create(nsize_src_filepath).expect("could not create file");
    }
}
