use structopt::{StructOpt};

mod verb;
use verb::*;

fn main() {
    match Verb::from_args() {
        Verb::Build(verb) => verb.run(),
        Verb::Run(verb) => verb.run(),
    };
}
