# ksr
ksr finds largest initial permutations when $n=18$ and $n=19$.
Based on https://www-cs-faculty.stanford.edu/~knuth/programs/topswops-fwd.w, ksr is written.

# Dependency
All experiments are done on Ubuntu 18.04 LTS.
- gcc == 7.5.0
- cargo == 1.45.0
- libsqlite3-dev == 3.22.0
- GNU Make == 4.1

```
# Install gcc, libsqlite3-dev, and make
sudo apt install gcc libsqlite3-dev make

# Install Rust. See https://www.rust-lang.org/tools/install.
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
It is recommended that you run this program on a terminal multiplexer such as `tmux`.

# Download 
```
git clone https://gitlab.com/kkimura/topswops.git
```

# Usage
Note that `PROJECT_HOME` denotes directory `./ksr`.

1. Set parameters and check the number of the nodes at level.  
The below command generates the nodes at the level 2 (the argument is 2+1) when $n=18$.
In this case, the number of nodes is 240 at level 2.
```
$ cd $PROJECT_HOME
$ cargo run build --leaf 3 --nsize 18
gcc setupper.c -Wall -lsqlite3  -o setupper -Wall -lsqlite3 
success: open sqlite file.
{"perm": [ 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18, 1],"card": [-3, 2, 3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16,-17, 0],"c": 2, "j": 15, "lv": 3, "max_unused": 18}
{
                .
                .
                .
           1 nodes at level 0.
          16 nodes at level 1.
         240 nodes at level 2.
           0 nodes at level 3.
           0 nodes at level 4.
           0 nodes at level 5.
           0 nodes at level 6.
           0 nodes at level 7.
           0 nodes at level 8.
           0 nodes at level 9.
           0 nodes at level 10.
           0 nodes at level 11.
           0 nodes at level 12.
           0 nodes at level 13.
           0 nodes at level 14.
           0 nodes at level 15.
           0 nodes at level 16.
           0 nodes at level 17.

gcc bts.c -Wall -pthread -O2 -lsqlite3  -o bts -Wall -pthread -O2 -lsqlite3 
```
The program generates sql file `topswops.sql`, directory `n18-lv3`, log file `n18-lv3/root-n18-lv3.log`.
- `topswops.sql` contains the arguments of the all node at level 2 when n=18.
  The argument contains `perm`, `card`, `c`, `j`, `lv` and `max_unused` of the parameters of function `next_node`.

2. Select the start index and the end index of the nodes, and run the below program to search from i(=1)-th node to i'(=88)-th node by d(=40) threads.
```
$ cargo run run --delta 40 --ith 1 --idth 88
```
3. The below command output the largest permutations.
```
$ grep count $PROJECT_HOME/cbts/n18-lv3/*.log
```
For each i-th node, the program generates the log file and the json file.
A filename with $n=18$, level 2 and 44th node denotes `n18-lv3-44.log` (resp. `n18-lv3-44.json`).
A log file contains the largest permutation, elapsed time, and the number of searching nodes.
A json file contains the frequency for each numbers of topswops.
