#include <bits/types/clockid_t.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include "../inc/type_ts.h"
#include "../inc/log_bts.h"
#include "../inc/ts_sqlite3_reader.h"

unsigned char score[]= {
  0,0,1,2,4,7,10,16,22,30,38,51,65,80,101,113,139,159,
  191,221,249,
};

long profile[NSIZE+1];
long iteration;
const long iteration_threshold = 1000000000;
static FILE* logfile_pointer;
static int ith;

void topswops(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num, clockid_t cid);

void next_perm(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num, clockid_t cid) {
  int t, k, c;
  for (int p_i = j-1; p_i >= 0; p_i--) {
    k = perm.q[NSIZE-2-p_i];
    if (k == -cards.q[0]) {
      continue;
    }
    c = count + 1;

    if (k == max_unused_num) {
      for (t = 1; cards.q[t] == k-t; t++)
        ;
      if (c + score[k-t] < score[NSIZE]) {
        continue;
      }
    } else if (max_unused_num < NSIZE && c + score[max_unused_num] < score[NSIZE]) {
      continue;
    }

    if (lv <= LEAF_LEVEL + 2 || iteration == iteration_threshold) {
      print_in_iteration(profile[lv], lv, cid, ith, logfile_pointer);
    }
    if (iteration == iteration_threshold) {
      iteration = 0;
    }
    iteration++;
    topswops(perm, cards, c, p_i, lv, max_unused_num, cid);
  }
}

void topswops(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num, clockid_t cid) {
  int k = perm.q[NSIZE-2-j];
  perm.q[NSIZE-2-j] = perm.q[lv-1];
  perm.q[lv-1] = k;

  // topswops until unknown card is head.
  while (1) {
    cards.q[0] = cards.q[k-1];
    cards.q[k-1] = k;

    int t, wj = 1;
    for (wj = 1, k -= 2; wj < k; wj++, k--) {
      t = cards.q[wj], cards.q[wj] = cards.q[k], cards.q[k] = t;
    }
    k = cards.q[0];
    if (k <= 0) {
      break;
    }
    count++;
  }
  profile[lv]++;

  if (lv == NSIZE-1) {
    add(perm, count);
    if (count >= score[NSIZE]) {
      score[NSIZE] = count;
      print_solution(perm, cards, count, ith, logfile_pointer);
    }
    return;
  }

  // find previous number on nonfixed.
  int t;
  for (t = max_unused_num; cards.q[t-1] == t; t--)
    ;
  max_unused_num = t;
  lv++;
  j = NSIZE - lv;

  next_perm(perm, cards, count, j, lv, max_unused_num, cid);
}

void search_from_root() {
  w_seq perm, cards;
  for (int k = 1; k < NSIZE; k++) {
    perm.q[k-1] = k+1;
    cards.q[k-1] = -k;
  }
  perm.q[NSIZE-1] = 1;
  int j = NSIZE-1;
  int count = 0;
  int level = 1;
  profile[0] = 1;

  // 計測開始
  clockid_t cid;
  pthread_getcpuclockid(pthread_self(), &cid);

  next_perm(perm, cards, count, j, level, NSIZE, cid);

  for(int k = 0; k < NSIZE; k++) {
    printf("%12ld nodes at level %d.\n", profile[k],k);
  }
  // print_clock("Main thread CPU time:   ", cid);
  printf("\n");
}

int main(int argc, char** argv) {
  if (argc < 4) {
    fprintf(stderr, "error: few arguments;");
    exit(EXIT_FAILURE);
  }

  int ith = atoi(argv[1]);
  if (ith == 0) {
    fprintf(stderr, "error: ith >= 1.");
    exit(EXIT_FAILURE);
  }

  if ((logfile_pointer = fopen(argv[2], "w")) == NULL) {
    fprintf(stderr, "error: cannot open log file.\n");
    exit(EXIT_FAILURE);
  }
  FILE* json_pointer;
  if ((json_pointer = fopen(argv[3], "w")) == NULL) {
    fprintf(stderr, "error: cannot open json file.\n");
    exit(EXIT_FAILURE);
  }

  topswops_sqlite3_connect("./topswops.sql", LEAF_LEVEL);
  StartNode node = read_ith_node(ith, LEAF_LEVEL);
  topswops_sqlite3_close();

  w_seq perm = node.perm;
  w_seq cards = node.cards;
  int j = node.j;
  int count = node.count;
  int lv = node.lv;
  int max_unused_num = node.max_unused_num;
  profile[0] = 0;

  init(100);

  print_perm(perm);
  print_card(cards);
  fprintf(stderr,
    "ith: %d, j:%d, c: %d, lv:%d, maxunused:%d\n",
    ith, j, count, lv, max_unused_num
  );

  // 計測開始
  clockid_t cid;
  pthread_getcpuclockid(pthread_self(), &cid);

  next_perm(perm, cards, count, j, lv, max_unused_num, cid);

  print_fn_data(json_pointer);
  print_clock("Main thread CPU time:   ", cid, logfile_pointer);
  for(int k = 0; k < NSIZE; k++) {
    fprintf(logfile_pointer, "%12ld nodes at level %d.\n", profile[k],k);
    fprintf(stderr, "%12ld nodes at level %d.\n", profile[k],k);
  }
  fclose(logfile_pointer);
  fclose(json_pointer);

  return 0;
}
