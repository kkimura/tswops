#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../inc/config_setup.h"
#include "../inc/log_setup.h"
#include "../inc/mysqlite3.h"

unsigned char score[]= {
  0,0,1,2,4,7,10,16,22,30,38,51,65,80,101,113,139,159,
  191,221,249,
};

long profile[NSIZE+1];
static FILE* logfile_pointer;

void topswops(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num);

void next_perm(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num) {
  int t, k, c;

  for (int p_i = j-1; p_i >= 0; p_i--) {
    k = perm.q[NSIZE-2-p_i];
    if (k == -cards.q[0]) {
      continue;
    }
    c = count + 1;

    if (k == max_unused_num) {
      for (t = 1; cards.q[t] == k-t; t++)
        ;
      if (c + score[k-t] < score[NSIZE]) {
        continue;
      }
    } else if (max_unused_num < NSIZE && c + score[max_unused_num] < score[NSIZE]) {
      continue;
    }

    topswops(perm, cards, c, p_i, lv, max_unused_num);
  }
}

void topswops(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num) {
  int k = perm.q[NSIZE-2-j];
  perm.q[NSIZE-2-j] = perm.q[lv-1];
  perm.q[lv-1] = k;

  // topswops until unknown card is head.
  while (1) {
    cards.q[0] = cards.q[k-1];
    cards.q[k-1] = k;

    int t, wj = 1;
    for (wj = 1, k -= 2; wj < k; wj++, k--) {
      t = cards.q[wj], cards.q[wj] = cards.q[k], cards.q[k] = t;
    }
    k = cards.q[0];
    if (k <= 0) {
      break;
    }
    count++;
  }
  profile[lv]++;

  // find previous number on nonfixed.
  int t;
  for (t = max_unused_num; cards.q[t-1] == t; t--)
    ;
  max_unused_num = t;
  lv++;
  j = NSIZE - lv;

  if (lv == LEAF_LEVEL) {
    topswops_insert_sqlite3(perm, cards, count, j, lv, max_unused_num);
    print_start_node(perm, cards, count, j, lv, max_unused_num, logfile_pointer);
    return;
  }

  next_perm(perm, cards, count, j, lv, max_unused_num);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "error: few arguments;");
    exit(EXIT_FAILURE);
  }

  w_seq perm, cards;
  for (int k = 1; k < NSIZE; k++) {
    perm.q[k-1] = k+1;
    cards.q[k-1] = -k;
  }
  perm.q[NSIZE-1] = 1;
  cards.q[NSIZE-1] = 0;
  int j = NSIZE-1;
  int count = 0;
  int lv = 1;
  int max_unused_num = NSIZE;
  profile[0] = 1;

  if ((logfile_pointer = fopen(argv[1], "w")) == NULL) {
    fprintf(stderr, "error: cannot open file.\n");
    exit(EXIT_FAILURE);
  }

  topswops_sqlite3_connect("./topswops.sql", LEAF_LEVEL);
  next_perm(perm, cards, count, j, lv, max_unused_num);
  topswops_sqlite3_close();

  for(int k = 0; k < NSIZE; k++) {
    fprintf(logfile_pointer, "%12ld nodes at level %d.\n", profile[k],k);
    printf("%12ld nodes at level %d.\n", profile[k],k);
  }
  fprintf(logfile_pointer, "\n");
  printf("\n");

  fclose(logfile_pointer);
}
