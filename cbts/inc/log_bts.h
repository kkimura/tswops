#ifndef LOG_BTS_H
#define LOG_BTS_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include "type_ts.h"

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

void print_clock(char *msg, clockid_t cid, FILE* fp) {
  struct timespec ts;

  fprintf(stderr, "%s", msg);
  fprintf(fp, "%s", msg);
  if (clock_gettime(cid, &ts) == -1) {
    handle_error("clock_gettime");
  }
  fprintf(stderr, "%4ld.%03ld sec\n", ts.tv_sec, ts.tv_nsec / 1000000);
  fprintf(fp, "%4ld.%03ld sec\n", ts.tv_sec, ts.tv_nsec / 1000000);
}

void print_in_iteration(long prof_lv, int lv, clockid_t cid, int ith, FILE* fp) {
  struct timespec ts;

  if (clock_gettime(cid, &ts) == -1) {
    handle_error("clock_gettime");
  }

  fprintf(fp, "%4ld at lv%2d :%4ld.%03ld sec\n", prof_lv, lv, ts.tv_sec, ts.tv_nsec / 1000000);
  fprintf(stderr, "%4ld at lv%2d :%4ld.%03ld sec\n", prof_lv, lv, ts.tv_sec, ts.tv_nsec / 1000000);
}

void print_clock_out_err(char* msg, clockid_t cid, FILE* fp) {
  struct timespec ts;

  fprintf(fp, "%s", msg);
  fprintf(stderr, "%s", msg);
  if (clock_gettime(cid, &ts) == -1) {
    handle_error("clock_gettime");
  }
  fprintf(fp, "%4ld.%03ld sec", ts.tv_sec, ts.tv_nsec / 1000000);
  fprintf(stderr, "%4ld.%03ld sec", ts.tv_sec, ts.tv_nsec / 1000000);
}

void print_perm(w_seq perm) {
  printf("perm: ");
  for (int kk = 0; kk < NSIZE; kk++)
    printf("%d ", perm.q[kk]);
  printf("\n");
}

void print_card(w_seq card) {
  printf("card: ");
  for (int kk = 0; kk < NSIZE; kk++)
    printf("%d ", card.q[kk]);
  printf("\n");
}

void print_solution(w_seq perm, w_seq cards, int count, int ith, FILE* fp) {
  fprintf(fp, "count: %3d;", count);
  fprintf(stderr, "count: %3d;", count);

  seq b, bb;
  for(int k = 1; k <= NSIZE; k++) {
    b[k-1] = -k;
  }
  // construct first permutation of cards.
  for(int k = 1; k <= NSIZE; k++) {
    while(b[0] > 0) {
      int j, c, t;
      j = b[0], b[0]= b[j-1], b[j-1]= j;
      for(c = 1, j-= 2; c < j; c++, j--) {
        t = b[c], b[c] = b[j], b[j] = t;
      }
    }
    bb[-b[0]-1] = perm.q[k-1];
    b[0]= perm.q[k-1];
  }

  for(int k = 0; k < NSIZE; k++) {
    fprintf(fp, " %d",bb[k]);
    fprintf(stderr, " %d",bb[k]);
  }
  fprintf(fp, " -> 1");
  fprintf(stderr, " -> 1");
  for(int k = 1; k < NSIZE; k++) {
    fprintf(fp, " %d", cards.q[k]);
    fprintf(stderr, " %d", cards.q[k]);
  }
  fprintf(fp, "\n");
  fprintf(stderr, "\n");
}

void print_fn_data(FILE* fp) {
  fprintf(fp, "{");
  fprintf(stderr, "{");
  int not_found_next_node = 0;
  for (int i = NSIZE-1; i < FN_DATA_MAX_SIZE; i++) {
    PermLinkedListNode* plln = data_array[i].head;
    if (plln == NULL) {
      if (not_found_next_node && i == FN_DATA_MAX_SIZE-1) {
        fprintf(fp, "]}\n");
        fprintf(stderr, "]}\n");
      }
      continue;
    }
    if (not_found_next_node) {
      fprintf(fp, "]},\n");
      fprintf(stderr, "]},\n");
      not_found_next_node = 0;
    }

    fprintf(fp, "\"%d\": {\"frequency\": %lld, \"entry\": %d,\"kperm\": [", i, get_frequency(i), get_entry(i));
    fprintf(stderr, "\"%d\": {\"frequency\": %lld, \"entry\": %d, \"kperm\": [", i, get_frequency(i), get_entry(i));

    for (; plln != NULL; plln = plln->next) {
      fprintf(fp, "[");
      fprintf(stderr, "[");
      for (int kk = 0; kk < NSIZE; kk++) {
        if (kk == NSIZE - 1) {
          fprintf(fp, "%d", plln->seq.q[kk]);
          fprintf(stderr, "%d", plln->seq.q[kk]);
        } else {
          fprintf(fp, "%d,", plln->seq.q[kk]);
          fprintf(stderr, "%d,", plln->seq.q[kk]);
        }
      }
      if (plln->next == NULL) {
        fprintf(fp, "]");
        fprintf(stderr, "]");
      } else {
        fprintf(fp, "],");
        fprintf(stderr, "],");
      }
    }
    if (i+1 == FN_DATA_MAX_SIZE || data_array[i+1].head == NULL) {
      not_found_next_node = 1;
      continue;
    } else {
      fprintf(fp, "]},\n");
      fprintf(stderr, "]},\n");
    }
  }

  fprintf(fp, "}\n");
  fprintf(stderr, "}\n");
}

#endif /* LOG_BTS_H */
