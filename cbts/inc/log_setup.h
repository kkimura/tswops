#ifndef LOG_SETUP_H
#define LOG_SETUP_H

#include <stdio.h>
#include <stdlib.h>
#include "type_ts.h"

void print_start_node(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num, FILE* logfile_pointer) {
  printf("{\"perm\": [" );
  fprintf(logfile_pointer, "{\"perm\": [");
  for (int kk = 0; kk < NSIZE; kk++) {
    printf("%2d,", perm.q[kk]);
    fprintf(logfile_pointer, "%2d,", perm.q[kk]);
  }
  printf("\b],");
  fprintf(logfile_pointer, "\b],");

  printf("\"card\": [");
  fprintf(logfile_pointer, "\"card\": [");
  for (int kk = 0; kk < NSIZE; kk++) {
    printf("%2d,", cards.q[kk]);
    fprintf(logfile_pointer, "%2d,", cards.q[kk]);
  }
  printf("\b],");
  fprintf(logfile_pointer, "\b],");
  printf("\"c\": %d, \"j\": %d, \"lv\": %d, \"max_unused\": %d}\n", count, j, lv, max_unused_num);
  fprintf(logfile_pointer, "\"c\": %d, \"j\": %d, \"lv\": %d, \"max_unused\": %d}\n", count, j, lv, max_unused_num);
  fflush(stdout);
  fflush(logfile_pointer);
}

void print_perm(w_seq perm) {
  printf("perm: ");
  for (int kk = 0; kk < NSIZE; kk++)
    printf("%d ", perm.q[kk]);
  printf("\n");
}

void print_card(w_seq card) {
  printf("card: ");
  for (int kk = 0; kk < NSIZE; kk++)
    printf("%d ", card.q[kk]);
  printf("\n");
}

#endif /* LOG_SETUP_H */
