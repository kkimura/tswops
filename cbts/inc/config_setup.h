#ifndef CONFIG_SETUP_H
#define CONFIG_SETUP_H

#include "type_ts.h"

typedef struct InputNode {
  w_seq perm;
  w_seq cards;
  int leaf_level;
} InputNode;

#endif /* CONFIG_SETUP_H */
