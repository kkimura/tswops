#ifndef MYSQLITE3_H
#define MYSQLITE3_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#include "log_setup.h"
#include "type_ts.h"

static int print_resp(
  void *get_prm   , // sqlite3_exec()の第4引数
  int col_cnt     , // 列数
  char **row_txt  , // 行内容
  char **col_name   // 列名
){
  printf( "%s : %s\n" , row_txt[0] , row_txt[1] );
  return 0;
}

static sqlite3* conn = NULL;

void topswops_sqlite3_connect(const char* filepath, int lv) {
  if (SQLITE_OK != sqlite3_open(filepath, &conn)) {
    fprintf(stderr, "error: cannot open sqlite file.\n");
    exit(EXIT_FAILURE);
  }
  fprintf(stderr, "success: open sqlite file.\n");

  char stmt[512] = {};
  char *err_msg;

  snprintf(
    stmt, sizeof(stmt),
    "DROP TABLE IF EXISTS 'n%d-lv%d';", NSIZE, lv
  );
  int ret = sqlite3_exec(conn,stmt,print_resp,NULL,&err_msg);

  snprintf(
    stmt, sizeof(stmt),
    "CREATE TABLE `n%d-lv%d` (`ith`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`perm`	TEXT,`cards`	TEXT,`count`	INTEGER,`j`	INTEGER,`lv`	INTEGER,`maxunused`	INTEGER);",
    NSIZE, lv
  );
  ret = sqlite3_exec(conn, stmt, print_resp, NULL, &err_msg);
  if (SQLITE_OK != ret) {
    sqlite3_close(conn);
    sqlite3_free(err_msg);
    fprintf(stderr, "error: cannot execute sql stament.\n");
    exit(EXIT_FAILURE);
  }
}

void topswops_sqlite3_close() {
  if (SQLITE_OK != sqlite3_close(conn)) {
    fprintf(stderr, "error: cannot close sqlite file.\n");
    exit(EXIT_FAILURE);
  }
  conn = NULL;
}

typedef struct StrStruct {
  char str[128];
} StrStruct;

StrStruct Chars2StrStruct(w_seq ws) {
  char tmp_i2cs[128] = {};
  for (int i = 0; i < NSIZE; i++) {
    char ith_elem[8];
    snprintf(ith_elem, sizeof(ith_elem), "%d,", ws.q[i]);
    strcat(tmp_i2cs, ith_elem);
  }
  int length = strlen(tmp_i2cs);
  StrStruct strstr;
  snprintf(strstr.str, length, "%s",tmp_i2cs);
  return strstr;
}

void topswops_insert_sqlite3(w_seq perm, w_seq cards, int count, int j, int lv, int max_unused_num) {
  char sql_str[512] = {};
  char *err_msg = NULL;

  StrStruct perm_strs = Chars2StrStruct(perm);
  StrStruct card_strs = Chars2StrStruct(cards);

  snprintf(
    sql_str, sizeof(sql_str),
    "INSERT INTO 'n%d-lv%d'(perm,cards,count,j,lv,maxunused) VALUES('%s','%s',%d,%d,%d,%d);",
    NSIZE, lv,
    perm_strs.str, card_strs.str, count, j, lv, max_unused_num
  );
  int ret = sqlite3_exec(
    conn,
    sql_str,
    print_resp,
    NULL,
    &err_msg
  );

  if (SQLITE_OK != ret) {
    sqlite3_close(conn);
    sqlite3_free(err_msg);
    fprintf(stderr, "error: cannot execute sql stament.\n");
    exit(EXIT_FAILURE);
  }
}

#endif /* MYSQLITE3_H */
