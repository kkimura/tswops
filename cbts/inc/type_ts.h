//
// Don't edit this file!.
//

#ifndef TYPE_TS_H
#define TYPE_TS_H

#include <stdio.h>
#include <stdlib.h>

#define NSIZE 14
#define LEAF_LEVEL 3

typedef char seq[NSIZE+1];
typedef struct w_seq {
  seq q;
} w_seq;

typedef struct StartNode {
  w_seq perm;
  w_seq cards;
  int count;
  int j;
  int lv;
  int max_unused_num;
} StartNode;

typedef struct PermLinkedListNode PermLinkedListNode;

typedef struct PermLinkedListNode {
  w_seq seq;
  PermLinkedListNode* next;
} PermLinkedListNode;

typedef struct FnDataNode {
  PermLinkedListNode* head;
  int count;
  int current_entry;
  long long int destoried_node_num;
  int upper_bound;
} FnDataNode;

#define FN_DATA_MAX_SIZE 250 \

static FnDataNode data_array[FN_DATA_MAX_SIZE];

void init(int upperbound) {
  for (int i = 0; i < FN_DATA_MAX_SIZE; i++) {
    FnDataNode n;
    n.head = NULL;
    n.count = 0;
    n.current_entry = 0;
    n.destoried_node_num = 0;
    n.upper_bound = upperbound;
    data_array[i] = n;
  }
}

int get_entry(int i) {
  return data_array[i].current_entry;
}

long long int get_frequency(int i) {
  return ((long long int)data_array[i].current_entry) + data_array[i].destoried_node_num;
}

void add(w_seq perm, int count) {
  if (data_array[count].current_entry >= data_array[count].upper_bound) {
    data_array[count].destoried_node_num += 1;
    return;
  }
  PermLinkedListNode* new_node = (PermLinkedListNode*)malloc(sizeof(PermLinkedListNode));
  new_node->seq = perm;
  new_node->next = data_array[count].head;

  data_array[count].head = new_node;
  data_array[count].current_entry += 1;
  data_array[count].count = count;
}

#endif /* TYPE_TS_H */
