#ifndef TS_SQLITE3_READER_H
#define TS_SQLITE3_READER_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sqlite3.h>

#include "log_bts.h"
#include "type_ts.h"

static sqlite3* conn = NULL;


void topswops_sqlite3_connect(const char* filepath, int lv) {
  if (SQLITE_OK != sqlite3_open(filepath, &conn)) {
    fprintf(stderr, "error: cannot open sqlite file.\n");
    exit(EXIT_FAILURE);
  }
  fprintf(stderr, "success: open sqlite file.\n");
}

void topswops_sqlite3_close() {
  if (SQLITE_OK != sqlite3_close(conn)) {
    fprintf(stderr, "error: cannot close sqlite file.\n");
    exit(EXIT_FAILURE);
  }
  conn = NULL;
}

char** split(char* a_str, const char a_delim) {
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp) {
        if (a_delim == *tmp) {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = (char**)malloc(sizeof(char*) * count);

    if (result) {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

static int callback_read_record(void* data, int argc, char **argv, char** azColName) {
  StartNode* n = (StartNode*)data;

  for (int i = 1; i < argc; i++) {
    if (strcmp(azColName[i], "perm") == 0) {
      char** dst = split(argv[i], ',');
      for (int k = 0; k < NSIZE; k++) {
        n->perm.q[k] = atoi(dst[k]);
      }
    } else if (strcmp(azColName[i], "cards") == 0) {
      char** dst = split(argv[i], ',');
      int tmp_ints = 0;
      for (int k = 0; k < NSIZE; k++) {
        tmp_ints = atoi(dst[k]);
        n->cards.q[k] = (char)tmp_ints;
      }
      fprintf(stderr, "\n");
    } else if (strcmp(azColName[i], "count") == 0) {
      n->count = atoi(argv[i]);
    } else if (strcmp(azColName[i], "j") == 0) {
      n->j = atoi(argv[i]);
    } else if (strcmp(azColName[i], "lv") == 0) {
      n->lv = atoi(argv[i]);
    } else if (strcmp(azColName[i], "maxunused") == 0) {
      n->max_unused_num = atoi(argv[i]);
    } else {
      fprintf(stderr, "error: parsing column name %s\n", azColName[i]);
      exit(EXIT_FAILURE);
    }
  }
  return 0;
}

StartNode read_ith_node(int ith, int lv) {
  char sql_str[512] = {};
  char *err_msg = NULL;
  StartNode node;

  snprintf(
    sql_str, sizeof(sql_str),
    "SELECT * FROM `n%d-lv%d` WHERE ith = %d;", NSIZE, lv, ith
  );
  int ret = sqlite3_exec(
    conn,
    sql_str,
    callback_read_record,
    (void*)(&node),
    &err_msg
  );
  if (SQLITE_OK != ret) {
    sqlite3_close(conn);
    sqlite3_free(err_msg);
    fprintf(stderr, "error: fail to read.\n");
    exit(EXIT_FAILURE);
  }
  return node;
}

#endif /* TS_SQLITE3_READER_H */
